
const College = require('./model')
const resolvers = {
  Mutation: {
    createCollege: async (parent, args, context, info) => {
      const { sUniversityId, sName, sAddress } = args.college
      console.log(sUniversityId, sName, sAddress)
      const college = await College.create({ sUniversityId, sName, sAddress })
      return college
    }
  }
}

module.exports = resolvers
