const mongoose = require('mongoose')

const collegeSchema = new mongoose.Schema({
  sUniversityId: mongoose.Types.ObjectId,
  sName: String,
  sAddress: String
})

module.exports = mongoose.model('College', collegeSchema)
