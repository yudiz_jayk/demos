const collegeTypedefs = require('./College/collegeTypeDefs')
const collegeResolver = require('./College/collegeRes')
const UniversityTypedef = require('./University/universityTypeDefs')
const universityResolver = require('./University/universityRes')
const { gql } = require('apollo-server-express')

const typeDefs = gql`
 ${collegeTypedefs}
 ${UniversityTypedef}
`
const resolvers = {
  ...collegeResolver,
  ...universityResolver
}

module.exports = { typeDefs, resolvers }
