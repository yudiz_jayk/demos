const mongoose = require('mongoose')

const universitySchema = new mongoose.Schema({
  sName: String,
  eType: {
    type: String,
    enum: ['Private', 'Goverment'],
    default: 'Goverment'
  },
  sAddress: String,
  dDate: String,
  sEmail: String
})

module.exports = mongoose.model('University', universitySchema)
