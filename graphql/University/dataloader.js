const DataLoader = require('dataloader')
const College = require('../College/model')
const { groupBy, map } = require('ramda')

module.exports.collegesDataLoader = function collegesDataLoader () {
  return new DataLoader(collegesByUniversity)
}

async function collegesByUniversity (bookIds) {
  console.log('Data Loaders')
  const colleges = await College.find({ sUniversityId: { $in: bookIds } })
  const groupedById = groupBy(clg => clg.sUniversityId, colleges)
  return map(bookId => groupedById[bookId], bookIds)
}
