const { RESTDataSource } = require('apollo-datasource-rest')

class APIS extends RESTDataSource {
  constructor () {
    super()
    this.baseURL = 'https://splitwisedev.herokuapp.com/api/user'
  }

  willSendRequest (request) {
    request.headers.set('authorization', 'Barear eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJfaWQiOiI2Mjk0NjYxZTg5MDYyYmQ1NzZhNTk1NGUiLCJpYXQiOjE2NTk1MjgyMDYsImV4cCI6MTY2MjEyMDIwNn0.6vGqD55fyyhYuNdQU7po5kUqwN24SD9ofUpElhfrbPc')
  }

  async getLoggedInUserGroups () {
    const result = await this.get('usergrouplist')

    return result.aGroups
  }

  async delUniversity (id) {
    const result = await this.delete(`deleteUniversity/${id}`)

    return result
  }
}

module.exports = APIS
