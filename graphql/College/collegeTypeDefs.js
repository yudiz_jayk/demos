const typeDefs = `
   type clg{
    sName:String,
    sAddress:String,
    sUniversityId:String
   }

   type Query{
     getClg:clg
    }
 
    input College{
     sName:String,
     sAddress:String,
     sUniversityId:String
    }
    
    type Mutation {
     createCollege(college:College):clg
    }
`

module.exports = typeDefs
