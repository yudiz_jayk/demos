const University = require('./model')
const ObjectId = require('mongoose').Types.ObjectId
const { emailResolver, dateResolver } = require('../scalers')
const { UserInputError } = require('apollo-server')

const resolvers = {
  Date: dateResolver,
  Email: emailResolver,

  Query: {
    getAllUniversity: async () => {
      return await University.find()
    },
    getUniversity: async (parent, args, context, info) => {
      if (ObjectId.isValid(args.id)) {
        return await University.findById(args.id)
      } else {
        throw new UserInputError(`Invalid UniversityId ${args.id} `)
      }
    },

    getAllUniversitybyDatasource: async (_, { id }, { dataSources }) => {
      return dataSources.rest.getUniversites()
    },

    getGroups: async (_, { id }, context) => {
      console.log('Fired')
      return context.dataSources.rest.getLoggedInUserGroups()
    }

  },
  University: {
    async colleges (parent, args, context) {
      const { loaders } = context
      const { collegesDataLoader } = loaders
      return collegesDataLoader.load(parent._id)
    }
  },
  Mutation: {
    createUniversity: async (parent, args, context, info) => {
      const { sName, sAddress, eType, dDate, sEmail } = args.university
      console.log(args.university)
      const university = await University.create({ sName, sAddress, eType, dDate, sEmail })
      return university
    }

  }

}

module.exports = resolvers
