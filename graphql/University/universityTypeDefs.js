
const typeDefs = `
    scalar Date
    scalar Email
    
    directive @upper on FIELD_DEFINITION 

    type University{
        id:ID
        sName:String @upper
        sAddress:String  
        eType:String,
        dDate:Date,
        sEmail:Email
        colleges:[Colleges]
    }
    
    type Colleges{
        sUniversityId:String
        sName:String,
        sAddress:String
    }

 
    type Query{
        getAllUniversity:[University],
        getUniversity(id:ID!):University  
        getAllUniversitybyDatasource:[University]
        getGroups:[Group]
       
       # deleteUniversitybyDatasource(id:ID!):String
       
    }

     

    

    type oFrom{
        sName:String
    }

    type transactions{
        iFrom:String
        oFrom:oFrom,
        iTo:String
    }

    type Group{
        iGroupId:String
        nNetAmount:String,
        groupTransaction:[transactions]
    }
    
    input UniInput{
        sName:String 
        sAddress:String
        eType:String,
        dDate:Date,
        sEmail:Email
    }

    
    
    type Mutation {
        createUniversity(sName:String,sAddress:String,eType:String,sEmail:Email): University
       
    }
`
module.exports = typeDefs
