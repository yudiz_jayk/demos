const { rule, inputRule, shield, allow } = require('graphql-shield')
const { AuthenticationError } = require('apollo-server')

const isAuthenticated = rule({ cache: 'no_cache' })(
  async (parent, args, ctx, info) => {
    if (!ctx.User) {
      return new AuthenticationError('Unauthorized acess denied')
    } else {
      return true
    }
  }
)

const isEmail = inputRule()(
  (yup) =>
    yup.object({
      sAddress: yup.string().email('Address should be an Email!').required()
    }),
  {
    abortEarly: false
  }
)

const permissions = shield({
  Query: {
    getGroups: isAuthenticated
    // getGroups:deny
  },
  Mutation: {
    createUniversity: isEmail
  }

}, {
  fallbackError: 'Something went wrong',
  allowExternalErrors: allow
})

module.exports = permissions
