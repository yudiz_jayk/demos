const express = require('express')
const app = express()
const { ApolloServer } = require('apollo-server')
const { typeDefs, resolvers } = require('./graphql')
const { makeExecutableSchema } = require('@graphql-tools/schema')
const upperDirectiveTransformer = require('./graphql/derectives')
const { ApolloServerPluginLandingPageLocalDefault } = require('apollo-server-core')
const REST = require('./graphql/REST')
const { applyMiddleware } = require('graphql-middleware')
const Permissions = require('./graphql/rulesPermisions')
const errorHandler = require('./graphql/errorHandler')
const { collegesDataLoader } = require('./graphql/University/dataloader')
const users = [{ name: 'Jay', role: 0 }]

require('./db/index')
app.use(express.json())

const createContext = ({ req }) => {
  const authToken = req.headers.authorization
  if (authToken !== undefined) {
    return {
      User: users[0],
      loaders: {
        collegesDataLoader: collegesDataLoader()
      }
    }
  } else {
    return null
  }
}
async function startApolloServer (typeDefs, resolvers) {
  let schema = makeExecutableSchema({
    typeDefs,
    resolvers
  })

  schema = applyMiddleware(schema, Permissions)
  schema = upperDirectiveTransformer(schema, 'upper')

  const server = new ApolloServer({
    schema,
    csrfPrevention: true,
    cache: 'bounded',
    plugins: [
      ApolloServerPluginLandingPageLocalDefault({ embed: true })
    ],
    middlewares: [Permissions],
    context: createContext,
    formatError: (err) => {
      return new Error(errorHandler(err))
    },
    dataSources: () => {
      return {
        rest: new REST()
      }
    }

  })

  const { url } = await server.listen()
  console.log(`🚀 Server ready at ${url}`)
}

startApolloServer(typeDefs, resolvers)

app.listen(8000, () => {
  console.log('app listien on port 8000')
})
