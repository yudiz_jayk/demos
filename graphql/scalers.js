
const { GraphQLScalarType, Kind } = require('graphql')
const { UserInputError } = require('apollo-server-express')

const scaler = {}

scaler.dateResolver = new GraphQLScalarType({
  name: 'Date',
  description: 'for date option',

  serialize (value) {
    return new Date(value).getHours()
  },

  parseValue (value) {
    return new Date(Number(value))
  },

  parseLiteral (ast) {
    if (ast.kind === Kind.INT) {
      return new Date(Number(ast.value))
    }
    return null
  }
})

scaler.emailResolver = new GraphQLScalarType({
  name: 'Email',
  description: 'For Email ',

  serialize (value) {
    return value
  },

  parseValue (value) {
    if (/^\w+([\\.-]?\w+)*@\w+([\\.-]?\w+)*(\.\w{2,3})+$/.test(value)) {
      console.log(value)
      return value.toLowerCase()
    }

    throw new UserInputError('Provided Email is not Valid')
  }
})

module.exports = scaler
