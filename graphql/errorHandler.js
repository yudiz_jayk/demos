const { AuthenticationError, UserInputError } = require('apollo-server-express')
const { GraphQLError } = require('graphql')

module.exports = (err) => {
  console.log(err)

  if (err.originalError instanceof AuthenticationError) {
    return err.originalError
  }

  if (err instanceof GraphQLError && err.extensions.exception.name === 'ValidationError') {
    return err.message
  }

  if (err.originalError instanceof UserInputError) {
    return err.originalError
  }

  return 'Something went wrong'
}
