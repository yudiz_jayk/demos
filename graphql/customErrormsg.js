
const { ApolloError } = require('apollo-server')
module.exports.myError = class MyError extends ApolloError {
  constructor (message, code) {
    super(message, code)

    Object.defineProperty(this, 'name', { value: 'MyError' })
  }
}
